// Package saucenao provides a wrapper to saucenao.coms' json api
package saucenao

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/dghubble/sling"
	"gopkg.in/go-playground/validator.v9"
)

var validate *validator.Validate

// newfileUploadRequest takes a file path as string and returns an *http.Request with error
// @param url string
// @param parmas map[string]string
// @param paramName
// @fileName string
// @return *http.Request, error
func newfileUploadRequest(uri string, params map[string]string, paramName string, fileInterface io.Reader, fileName string) (*http.Request, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, fileName)
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, fileInterface)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return req, err
}

// newFileUploadFromFile wraps newfileUploadRequest
func newFileUploadFromFile(uri string, params map[string]string, paramName string, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	fileName := filepath.Base(path)
	fileUpload, err := newfileUploadRequest(uri, params, paramName, file, fileName)
	if err != nil {
		return nil, err
	}

	return fileUpload, err
}

// newFileUploadFromInterface wraps newfileUploadRequest
//
// @param URL to send to
//
// @params
// map[string]string{
//		"name":         "file",
//		"filename":     fileName,
//		"Content-Type": mime,
//	}
//
// @param parameter name, usually "file"
//
// @param name of file to upload
//
// @param io.reader interface of file

func newFileUploadFromInterface(uri string, params map[string]string, paramName string, fileName string, file io.Reader) (*http.Request, error) {
	fileUpload, err := newfileUploadRequest(uri, params, paramName, file, fileName)
	if err != nil {
		return nil, err
	}
	return fileUpload, err

}

// Base URL for api calls
const baseURL = "https://saucenao.com/search.php?"

//key is saucenao apikey
const key = ""

// defaultResults is what is says
const defaultResults = 5

// numResults30s is the maximum number of results allowed by account type over 30 seconds
var numResults30s = 12

// numResults24h is the maximum number of results allowed by account type over 24 hours
var numResults24h = 300

// SauceResults is the results of a query to api
type SauceResults struct {
	// Header contains information about user, api limits, and indexes.
	Header struct {
		UserID           interface{} `json:"user_id,omitempty"`
		AccountType      interface{} `json:"account_type,omitempty"`
		ShortLimit       string      `json:"short_limit"`
		LongLimit        string      `json:"long_limit"`
		LongRemaining    int         `json:"long_remaining"`
		ShortRemaining   int         `json:"short_remaining"`
		Status           int         `json:"status"`
		ResultsRequested int         `json:"results_requested"`

		Index struct {
			Status   int `json:"status"`
			ParentID int `json:"parent_id"`
			ID       int `json:"id"`
			Results  int `json:"results"`
		} `json:"index,omitempty"`

		SearchDepth       string  `json:"search_depth"`
		MinimumSimilarity float64 `json:"minimum_similarity"`
		QueryImageDisplay string  `json:"query_image_display"`
		QueryImage        string  `json:"query_image"`
		ResultsReturned   int     `json:"results_returned"`
	} `json:"header"`
	// Results is data returned about search results from saucenao
	Results []Results `json:"results"`
}

// Results is data returned about search results from saucenao
type Results struct {
	Header struct {
		Similarity string `json:"similarity"`
		Thumbnail  string `json:"thumbnail"`
		IndexID    int    `json:"index_id"`
		IndexName  string `json:"index_name"`
	} `json:"header,omitempty"`
	Data struct {
		DeviantartID   int `json:"da_id,omitempty"`
		GelbooruID     int `json:"gelbooru_id,omitempty"`
		YandereID      int `json:"yandere_id,omitempty"`
		PixivID        int `json:"pixiv_id,omitempty"`
		DanbooruID     int `json:"danbooru_id,omitempty"`
		SeigaID        int `json:"seiga_id,omitempty"`
		E621ID         int `json:"e621_id,omitempty"`
		SankakuID      int `json:"sankaku_id,omitempty"`
		MemberID       int `json:"member_id,omitempty"`
		AnidbAID       int `json:"anidb_aid,omitempty"`
		PawooID        int `json:"pawoo_id,omitempty"`
		MangadexID     int `json:"md_id,omitempty"`
		MangaUpdatesID int `json:"mu_id,omitempty"`
		MemberLinkID   int `json:"member_link_id,omitempty"`

		AuthorName string      `json:"author_name,omitempty"`
		AuthorURL  string      `json:"author_url,omitempty"`
		MemberName string      `json:"member_name,omitempty"`
		Title      string      `json:"title,omitempty"`
		ExtURLS    []string    `json:"ext_urls"`
		Creator    interface{} `json:"creator,omitempty"`
		Material   interface{} `json:"material,omitempty"`
		Source     interface{} `json:"source,omitempty"`
		Characters interface{} `json:"characters,omitempty"`
		Part       string      `json:"part,omitempty"`
		Year       string      `json:"year,omitempty"`
		EstTime    string      `json:"est_time,omitempty"`
		CreatedAt  time.Time   `json:"created_at,omitempty"`

		PawooUserAcct        string      `json:"pawoo_user_acct,omitempty"`
		PawooUserUsername    string      `json:"pawoo_user_username,omitempty"`
		PawooUserDisplayName string      `json:"pawoo_user_display_name,omitempty"`
		Type                 interface{} `json:"type,omitempty"`
		BcyType              string      `json:"bcy_type,omitempty"`
		EngName              string      `json:"eng_name,omitempty"`
		JpName               string      `json:"jp_name,omitempty"`
	} `json:"data,omitempty"`
}

// ResultsService is a method to get results from saucenao
type ResultsService struct {
	sling *sling.Sling
}

// NewResultsService returns a new ResultsService.
func NewResultsService(httpClient *http.Client) *ResultsService {
	return &ResultsService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// ImageSearchByURL searches saucenao by url and returns results
// If numResults is set to 0 it defaults to defaultResults
// @param apiKey string
// @param uri string
// @param numResults int
// @return SauceResults
// @return *http.Response
// @return error
func (s *ResultsService) ImageSearchByURL(apiKey string, numResults int, uri string) (SauceResults, *http.Response, error) {
	// Make sure we have a URL
	validate = validator.New()
	errs := validate.Var(uri, "required,url")
	if errs != nil {
		log.Fatalf("Input is not a URL, errs: %s", errs)
	}
	// set number of results if set to 0
	if numResults == 0 {
		numResults = defaultResults
	}

	results := new(SauceResults)
	var snErr error
	finalURL := fmt.Sprintf("%soutput_type=2&db=999&api_key=%s&numres=%v&url=%s", baseURL, apiKey, numResults, url.QueryEscape(uri))
	fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// ImageSearchByFile searches saucenao by file upload and returns results
// If numResults is set to 0 it defaults to defaultResults
// @param apiKey string
// @param numResults int
// @param filePath string
// @return SauceResults *http.Response error
func (s *ResultsService) ImageSearchByFile(apiKey string, numResults int, filePath string) (SauceResults, *http.Response, error) {
	// set number of results if set to 0
	if numResults == 0 {
		numResults = defaultResults
	}
	results := new(SauceResults)
	var snErr error
	extraParams := map[string]string{
		//"name":         "file",
		//"filename":     fileName,
		//"Content-Type": mime,
	}
	finalURL := fmt.Sprintf("%soutput_type=2&db=999&api_key=%s&numres=%v", baseURL, apiKey, numResults)
	fmt.Printf("FinalURL: %s\n", finalURL)
	fileUpload, err := newFileUploadFromFile(finalURL, extraParams, "file", filePath)
	if err != nil {
		log.Fatal("FileUpload:", err)
	}

	resp, err := s.sling.New().Do(fileUpload, results, snErr)
	if err != nil {
		fmt.Printf("ImageSearchByFile: Error reading response body: %v\n", err)
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// ImageSearchByInterface searches saucenao by file upload and returns results
// If numResults is set to 0 it defaults to defaultResults
// @param apiKey string
// @param numResults int
// @param file io.Reader
// @param fileName name of file string
// @return SauceResults *http.Response error
func (s *ResultsService) ImageSearchByInterface(apiKey string, numResults int, file io.Reader, fileName string) (SauceResults, *http.Response, error) {
	// set number of results if set to 0
	if numResults == 0 {
		numResults = defaultResults
	}
	results := new(SauceResults)
	var snErr error
	extraParams := map[string]string{
		//"name":         "file",
		//"filename":     fileName,
		//"Content-Type": mime,
	}
	finalURL := fmt.Sprintf("%soutput_type=2&db=999&api_key=%s&numres=%v", baseURL, apiKey, numResults)
	fmt.Printf("FinalURL: %s\n", finalURL)
	fileUpload, err := newFileUploadFromInterface(finalURL, extraParams, "file", fileName, file)
	if err != nil {
		log.Fatal("FileUpload:", err)
	}

	resp, err := s.sling.New().Do(fileUpload, results, snErr)
	if err != nil {
		fmt.Printf("ImageSearchByInterface: Error reading response body: %v\n", err)
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// Client to wrap services

// Client for saucenao
type Client struct {
	ResultsService *ResultsService
}

// NewClient returns a new Client
func NewClient(httpClient *http.Client) *Client {
	return &Client{
		ResultsService: NewResultsService(httpClient),
	}
}

/*func main() {
	client := NewClient(nil)
	sauceResults, _, _ := client.ResultsService.ImageSearchByFile("", defaultResults, "image.png")
	//sauceResults, _, _ := client.ResultsService.ImageSearchByURL("", 5, "https://img3.saucenao.com/booru/c/9/c96acca2aadbdb6bf259c732d2884744_4.jpg")
	//fmt.Printf("\n%#v\n", sauceResults.Header.Index)
	fmt.Printf("24h Limit: %v/%v\n", sauceResults.Header.LongRemaining, sauceResults.Header.LongLimit)
	fmt.Printf("30s Limit: %v/%v\n", sauceResults.Header.ShortRemaining, sauceResults.Header.ShortLimit)
	fmt.Println("--------------------------------------------------")

	for i := range sauceResults.Results {
		fmt.Printf("Index Name: %s\n", sauceResults.Results[i].Header.IndexName)
		fmt.Printf("Similarity percentage: %v\n", sauceResults.Results[i].Header.Similarity)
		fmt.Printf("URL[s]: %v\n", sauceResults.Results[i].Data.ExtURLS)
		fmt.Println("--------------------------------------------------")

	}

	//fmt.Println("Error, ", err)
	//fmt.Println("RESP: ", resp)
	//fmt.Printf("\nResults: \n%v\n", SauceResults)

}*/
