module gitlab.com/pointerofnull/saucenao-go/saucenao

go 1.14

require (
	github.com/dghubble/sling v1.3.0
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
